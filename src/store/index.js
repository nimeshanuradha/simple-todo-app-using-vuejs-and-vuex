import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    todos: [
      { id: 1, title: "One", status: "Done" },
      { id: 2, title: "Two", status: "Pending" },
      { id: 3, title: "Three", status: "Done" },
      { id: 4, title: "Four", status: "Pending" },
    ]
  },
  getters: {
    TODOS: (state) => {
      return state.todos
    }
  },
  mutations: {
    ADD_TODO: (state, playload) => {
      state.todos.push(playload)
    }
  },
  actions: {
    ADD_TODO: (context, playload) => {

      /* 
      Perform actions before commiting the mutation.
      You can do any processing task here like API requests etc.
      */
      playload.id = context.state.todos.length + 1

      // Commit the mutation after processing.
      context.commit("ADD_TODO", playload)
    },

    MARK_AS_DONE: (context, playload) => {
      context.getters.TODOS.forEach(todo => {
        if (todo.id == playload) {
          todo.status = "Done"
        }
      })      
    },
    DELETE_TODO: (context, playload) => {
      context.state.todos.forEach(todo => {
        if (todo.id == playload) {
          context.getters.TODOS.splice(context.state.todos.indexOf(todo),1)
        }
      })      
    }
  },
  modules: {


  }
})
